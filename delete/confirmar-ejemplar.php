<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Confirmacion de eliminacion de Ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $clave = $_GET['clave_ejemplar'];
  $isbn = $_GET['isbn'];
  
  $error = false;
  
  if (empty($clave)) {
    $error = true;
?>
  <p>Error, no se ha indicado la clave del ejemplar</p>
<?php
  }
  
  if( empty($isbn) ){
   $error=true;
  ?>
  <p>Error, no se especifico el isbn del ejemplar </p>
  <?php 
  }
   
  if(! $error ) {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select * from biblioteca.ejemplar
      where clave_ejemplar = '$clave' and isbn ='$isbn';";

    $libro = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($libro) == 0) {
      $error = true;
?>
  <p>No se ha encontrado la clave del ejemplar o el ISBN del ejemplar <?php echo $id_autor; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($libro, null, PGSQL_ASSOC);
?>
<table>
  <caption>Información de Ejemplar</caption>
  <tbody>
    <tr>
      <th>Clave de Ejemplar</th>
      <td><?php echo $clave; ?></td>
    </tr>
    <tr>
      <th>ISBN</th>
      <td><?php echo $isbn; ?></td>
    </tr>
    <tr>
      <th>Libro/os</th>
      <td>
<?php
  
      $query = "select titulo_libro from biblioteca.libro natural join biblioteca.ejemplar
 	  where isbn ='$isbn'";

      $libros = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($libros) == 0) {
?>
        <p>Sin Libros</p>
<?php
      }else{
?>
        <ol>
<?php
        while ($tupla = pg_fetch_array($libros, null, PGSQL_ASSOC)) {
			
          foreach ($tupla as $atributo) {
?>
          <li><?php echo $atributo; ?></li> 
<?php
          
		  }
		}
	   }
	}
  }
?>
        </ol>

  	  </td>

    </tr>
    
  </tbody>
</table>

<?php
  pg_free_result($result);
  pg_close($dbconn);

  if (!$error) {
?>
<form action="delete-ejemplar.php" method="post">
  <input type="hidden" name="isbn" value="<?php echo $isbn; ?>" />
  <input type="hidden" name="clave_ejemplar" value="<?php echo $clave; ?>" />
  <p>¿Está seguro/a de eliminar este Ejemplar?</p>
  <input type="submit" name="submit" value="DELETE" />
  <p>
    Se borrarán a su vez todos los ejemplares en existencia.
   </p>
</form>

<form action="listar-ejemplares.php" method="post">
  <input type="submit" name="submit" value="Cancelar" />
</form>
<?php
 
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="listar-ejemplares.php">Lista de Ejemplares</a></li>
</ul>

</body>
</html>
