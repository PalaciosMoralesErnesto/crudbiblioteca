<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de Ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<?php
  $clave_ejemplar = $_POST['clave_ejemplar'];
  $isbn = $_POST['isbn'];
  $error=false;
  
//  echo $isbn;
//  echo "<br>";
// echo $clave_ejemplar;
  
  if(empty($isbn)){
	 $error=true;
  ?>
  <p> No se especifico el ISBN del Ejemplar </p>
  <?php
  }
  
  if (empty($clave_ejemplar)) {
	 $error=true;
?>
  <p>Error, no se especifico la clave del ejemplar</p>
<?php
  } 
  
  if( ! $error ) {
   
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select * from biblioteca.ejemplar where clave_ejemplar = '$clave_ejemplar'";

    $autores = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($autores) == 0) {
?>
  <p>No se ha encontrado algún la clave del ejemplar <?php echo $id_autor; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($autores, null, PGSQL_ASSOC);

 
	  $query = "delete from biblioteca.ejemplar where clave_ejemplar='$clave_ejemplar' and isbn='$isbn' ;";
	  
      $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());
	  
	  
      if (pg_affected_rows($resultado) == 0) {
?>
  <p>Error al momento de borrar el Ejemplar</p>
<?php
      } else {
?>
  <p>El Ejemplar con la clave <?php echo $clave_ejemplar; ?> y ISBN "<?php echo $isbn; ?>" fue borrado con exito.</p>
<?php
      }
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="listar-ejemplares.php">Lista de Ejemplares</a></li>
</ul>

</body>
</html>