<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de Ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<?php

$isbn = $_GET['isbn'];
$clave = $_GET['clave_ejemplar'];


?>

<form action="update-ejemplar.php" method="post">
<table>
  <caption>Información de Ejemplares</caption>
  <tbody>
    <tr>
      <th>Clave Ejemplar</th>
      <td><input name="clave_ejemplar" type="text" value="<?php echo $clave; ?>" readonly /></td>
    </tr>
    <tr>
      <th>Isbn</th>
      <td><input name="isbn" value="<?php echo $isbn?>" readonly></input></td>
    </tr>
     <tr>
      <th>Conservacion</th>
      <td><input name="conservacion" > </input></td>
    </tr>
  </tbody>
</table>
<input type="submit" name="submit" value="Actualizar" />
</form>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
</ul>

</body>
</html>