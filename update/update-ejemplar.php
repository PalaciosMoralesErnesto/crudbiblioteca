<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de Autor</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<?php
  $error = false;
  $clave_ejemplar = $_POST['clave_ejemplar'];
  $isbn = $_POST['isbn'];
  $conservacion = $_POST['conservacion'];

  if (empty($clave_ejemplar)) {
    $error = true;
?>
  <p>Error, no se indico el la clave del Ejemplar</p>
<?php
  }
  if (empty($isbn)) {
    $error = true;
?>
  <p>Error, no se indico el isbn del Ejemplar</p>
<?php
  }
  
  if(empty($conservacion)){
	$error=true;
?>
 <p>Erro, no se especifcico el tipo de conservacion </p>
<?php	  
   }

  if (!$error) {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select * from biblioteca.ejemplar where clave_ejemplar = '".$clave_ejemplar."'
	and isbn='$isbn';";

    $autores = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($autores) == 0) {
?>
  <p>No se ha encontrado algún Ejemplar con clave <?php echo $clave_ejemplar; ?> y ISBN <?php echo $isbn; ?></p>
<?php
    } else {
      $query = "update biblioteca.ejemplar
        set conservacion_ejemplar = '".$conservacion."'
        where clave_ejemplar = '".$clave_ejemplar."' and isbn ='$isbn';";

      $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());

      if (pg_affected_rows($resultado) == 0) {
?>
  <p>Error al momento de actualizar los datos del Ejemplar</p>
<?php
      } else {
?>
  <p>Los datos del Ejemplar con clave <?php echo $clave_ejemplar; ?> y ISBN <?php echo $isbn; ?>
   han sido actualizados con exito</p>
<?php
      }
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="listar-ejemplares.php">Lista de Ejemplares</a></li>
</ul>

</body>
</html>