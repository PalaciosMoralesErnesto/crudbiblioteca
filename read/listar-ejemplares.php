<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
<title>Datos del Ejemplar</title>
<link rel="stylesheet" type="text/css" href="../css/estilo.css" > 
</head>


<body>

<?php

$servidor = 'localhost';
$user='programador';
$contraseña = '12345';
$nombre_db= 'prueba';

$result = pg_connect("host=$servidor dbname=$nombre_db user=$user password=$contraseña ") 
or die('No se ha podoso conectar '.pg_last_error);

$query = 'select * from  biblioteca.ejemplar order by isbn, clave_ejemplar ;';

$resultado = pg_query( $query ) or die('La consulta falló: ' . pg_last_error());;
?>
<table border=0>
  <caption>Información de Ejemplar</caption>
<thead>
    <tr>
      <th>#</th>
      <th>Clave Ejemplar</th>
      <th>Consevacion</th>
      <th>ISBN</th>
      <th>Opcion</th>
    </tr>
  </thead>
<tbody>
<?php      
$contador=1;
while($tupla = pg_fetch_array($resultado, null, PGSQL_ASSOC)){
$isbn = $tupla['isbn'];

?>
<tr>
<td>  <?php echo $contador ++; ?> </td>
<?php

foreach ($tupla as $atributo) {
?>
<td>
<?php
 echo ($atributo); 
?>
</td>
<?php
}
?>
<td><a href="info-ejemplar.php?isbn=<?php echo $isbn; ?>">Info</a>
</td>
<tr>
<?php
}
?>
<tbody>
</table>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
</ul>

</body>
</html>