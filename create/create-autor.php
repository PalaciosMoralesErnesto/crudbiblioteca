<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>CREATE Autor</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<?php
  $error = false;
  $id = $_POST['id_autor'];
  $nombre_autor = $_POST['nombre_autor'];

  if (empty($id)) {
    $error = true;
?>
  <p>Error, no se indico el id del Autor</p>
<?php
  }
  if (empty($nombre_autor)) {
    $error = true;
?>
  <p>Error, no se indico el nombre del autor</p>
<?php
  }

  if (!$error) {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select id_autor
      from biblioteca.autor
      where id_autor = '".$id."';";

    $libro = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($libro) == 1) {
?>
  <p>Error, ya se encuentra registrado un autor con id <?php echo $id; ?></p>
<?php
    } else {
      $query = "insert into biblioteca.autor values('".$id."', '".$nombre_autor."');";

      $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());

      if (pg_affected_rows($resultado) == 0) {
?>
  <p>Error al momento de guardar los datos del libro</p>
<?php
      } else {
?>
  <p>El Autor con el ID <?php echo $id; ?> y nombre "<?php echo $nombre_autor; ?>" ha sido guardado con exito.</p>
<?php
      }
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="formulario-autor.php">Nuevo Autor</a></li>
</ul>

</body>
</html>